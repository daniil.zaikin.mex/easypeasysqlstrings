﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace EasyPeasySQLStrings
{
    class Program
    {
        // Takes a set of strings as input and has the user select one of them. Allows for the user's custom input.
        static string UserSelect(IEnumerable<string> stringSet)
        {
            Console.WriteLine("Type in the number corresponding to the value to select it, or press RETURN to enter a custom one.");
            IDictionary<int, string> stringMap = new Dictionary<int, string>();
            int counter = 1;
            foreach (string value in stringSet)
            {
                stringMap.Add(counter, value);
                Console.WriteLine($"{counter}. {value}");
                counter++;
            }

            string userInput = Console.ReadLine();
            string returnValue;
            if (userInput == "")
            {
                Console.WriteLine("Please type in your own custom value:");
                returnValue = Console.ReadLine();
            }
            else
            {
                returnValue = stringMap[Int32.Parse(userInput)];
            }

            Console.WriteLine($"Selected: {returnValue}");
            return returnValue;
        }
        static void Main(string[] args)
        {
            const string root = @"C:\inetpub\wwwroot";

            IList<string> subDirectories = Directory.GetDirectories(root).Where(folderName => folderName.ToLower().Contains("mex")).ToArray();

            Console.WriteLine("Found these MEXData Folders.");

            string folderPath = UserSelect(subDirectories);

            // Gonna do this in LINQ just for fun

            XDocument xml;

            try
            {
                xml = XDocument.Load(folderPath + @"\web.config");
            }
            catch
            {
                xml = XDocument.Load(folderPath + @"\Web.config");
            }

            var boldQuery = from additions in xml.XPathSelectElements("//connectionStrings/add")
                            where additions.Attribute("name").Value == "Bolt_Entities"
                            select additions.Attribute("connectionString").Value.ToString();

            var entitiesQuery = from additions in xml.XPathSelectElements("//connectionStrings/add")
                                where additions.Attribute("name").Value == "MEXEntities"
                                select additions.Attribute("connectionString").Value.ToString();

            // it wasn't actually that fun, never using LINQ again


            string boldConnectionString = boldQuery.Single();
            // We need this as a separate variable to put it back into the file
            IList<string> entitiesConnectionStringSplit = entitiesQuery.Single().Split("\'");
            string entitiesConnectionString = entitiesConnectionStringSplit[1];

            // For some reason I couldn't find the SQLEnum method in any Nuget library I tried to install, 
            // so just gonna use services and tack on SupportWizard at the beginning

            IEnumerable<ServiceController> services = ServiceController.GetServices().Where(service =>
                service.ServiceName.StartsWith("MSSQL$"));

            //gonna initialise with support wizard for convenience
            ICollection<string> servicesStrings = new List<string>(
                new string[] { "SUPPORTWIZARD\\davesql2014", 
                    "SUPPORTWIZARD\\sqlserver2016", "SUPPORTWIZARD\\sqlserver2017" });

            SqlConnectionStringBuilder boldConnection = new SqlConnectionStringBuilder(boldConnectionString);
            SqlConnectionStringBuilder mexConnection = new SqlConnectionStringBuilder(entitiesConnectionString);

            foreach (var service in services)
            {
                //Console.WriteLine(service.ServiceName.Split('$').Last());
                servicesStrings.Add($"(local)\\{service.ServiceName.Split('$').Last()}");
            }

            // Bunch of hardcoded user inputs, can very easily go wrong 
            // but ACCURACY is for COWARDS - we're after SPEED here!

            Console.WriteLine("Please select an SQL Server Instance:");
            string selectedDatabase = UserSelect(servicesStrings);

            Console.WriteLine("Using NT Auth? (y for yes, anything else for no)");
            string ntAuthInput = Console.ReadLine();
            bool ntAuthBool;
            if (ntAuthInput == "y")
            {
                ntAuthBool = true;
            }
            else
            {
                ntAuthBool = false;
            }

            Console.WriteLine("Please type in your SQL login (RETURN for sa):");
            string userName = Console.ReadLine();
            if (userName == "")
            {
                userName = "sa";
            }

            Console.WriteLine("Please type in your SQL password (RETURN for Admin123):");
            string password = Console.ReadLine();
            if (password == "")
            {
                password = "Admin123";
            }

            // Populate connection strings with the values acquired thus far to try to establish a server connection
            // !!!!!!!!NO ERROR HANDLING HERE!!!!!!!!!
            IEnumerable<SqlConnectionStringBuilder> connectionStringBuilders = new SqlConnectionStringBuilder[] { boldConnection, mexConnection };
            foreach (SqlConnectionStringBuilder connectionStringBuilder in connectionStringBuilders)
            {
                connectionStringBuilder["Data Source"] = selectedDatabase;
                connectionStringBuilder.Remove("Initial Catalog");
                connectionStringBuilder["User ID"] = userName;
                connectionStringBuilder["Password"] = password;
                connectionStringBuilder["Integrated Security"] = ntAuthBool;
                Console.WriteLine(connectionStringBuilder.ConnectionString);
            }

            // Use the current string to get a nice, little (or big) list of databases

            ICollection<string> databases = new List<string>();

            using (SqlConnection conn = new SqlConnection(boldConnection.ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", conn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            databases.Add(dr[0].ToString());
                        }
                    }
                }
            }

            Console.WriteLine("Please select a database:");
            string database = UserSelect(databases);

            foreach (SqlConnectionStringBuilder connectionStringBuilder in connectionStringBuilders)
            {
                connectionStringBuilder["Initial Catalog"] = database;
            }

            // Chuck all the stuff back into the doc. Much better than LINQ.

            XmlDocument webConfig = new XmlDocument();
            webConfig.Load(folderPath + @"\web.config");
            XmlNodeList nodes = webConfig.SelectNodes("/configuration/connectionStrings/add");

            foreach (XmlNode node in nodes)
            {
                XmlAttribute connectionStringAttribute = node.Attributes["connectionString"];
                XmlAttribute nameAttribute = node.Attributes["name"];

                if (nameAttribute.Value == "Bolt_Entities")
                {
                    connectionStringAttribute.Value = boldConnection.ConnectionString;
                }
                else if (nameAttribute.Value == "MEXEntities")
                {
                    connectionStringAttribute.Value = $"{entitiesConnectionStringSplit[0]}\'{mexConnection.ConnectionString}\'{entitiesConnectionStringSplit[2]}";
                }
                else
                {
                    throw (new Exception("can't find shit"));
                }
            }

            webConfig.Save(folderPath + @"\web.config");
        }
    }
}
